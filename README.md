# test_setup_app

My flutter test seetup including build variants and app exception reporting, test screenshots, and 
atdd(bdd) setup.

# Credit

My stuff, Fredrick Grott

# License

BSD clause 2 copyright 2019 Fredrick Grott


# Resources

[ozzie screenshot tests](https://github.com/bmw-tech/ozzie.flutter)

[gherkin dart runner dherkin2](https://github.com/prolutionsGmbH/dherkin2)

[flutter test driver setup](https://flutter.dev/docs/cookbook/testing/integration/introduction)

[atdd testing](https://medium.com/flutter-community/automated-testing-using-atdd-in-flutter-21d4d0cf5df6)