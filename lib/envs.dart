import 'package:meta/meta.dart';

enum BuildFlavor { production, development, staging }

BuildEnvironment get env => _env;
BuildEnvironment _env;

/*
  Seems no way to avoid having log setup and app exception reporting
  in each build variant file so this is the approach.

  email var for ap exception reportuing
 */
class BuildEnvironment {
  /// The backend server.
  final String baseUrl;
  final BuildFlavor flavor;
  final String email;

  // ignore: sort_constructors_first
  BuildEnvironment._init({this.flavor, this.baseUrl, this.email});

  /// Sets up the top-level [env] getter on the first call only.
  static void init({@required dynamic flavor, @required dynamic baseUrl,@required dynamic email}) =>
      _env ??= BuildEnvironment._init(flavor: flavor, baseUrl: baseUrl, email: email);
}
